#!/bin/sh

APP_PID=$(adb shell pidof org.sars.BlinkyLog)

adb logcat --pid $APP_PID
