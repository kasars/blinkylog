/*
 * SPDX-FileCopyrightText: 2024 Kåre Särs <kare.sars@iki.fi>
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program in a file called COPYING; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

import QtQuick
import QtQuick.Window
import QtCore
import QtQuick.Controls.Material
import sars.MobileHelpers

ApplicationWindow {
    id: blink
    width: 800
    height: 480
    visible: true
    title: qsTr("Blinky Log")


    readonly property color bgColor: helpers.systemIsDarkMode ? "black" : "white"
    readonly property color fgColor: helpers.systemIsDarkMode ? "white" : "black"
    color: bgColor
    MobileHelpers {
        id: helpers
        statusbarColor: blink.bgColor
    }
    Material.theme: helpers.systemIsDarkMode ? Material.Dark : Material.Light

    readonly property int pageMargins: 5

    property double blinksPer_kWh: 1000

    ListModel {
        id: pagesModel
        ListElement {
            requestUrl: "http://power.local/10min"
            secondsPerValue: 600
            label: qsTr("10 min interval")
        }
        ListElement {
            requestUrl: "http://power.local/min"
            secondsPerValue: 60
            label: qsTr("1 min interval")
        }
        ListElement {
            requestUrl: "http://power.local/sec"
            secondsPerValue: 1
            label: qsTr("1 sec interval")
        }
    }

    SwipeView {
        id: views
        anchors.fill: parent
        clip: true

        Repeater {
            model: pagesModel
            delegate: Graph {
                width: views.width
                height: views.height
                barColor: "#ff007f"
                backgroundColor: bgColor
                textColor: fgColor
                label: model.label
                updateValues: ListView.isCurrentItem
                requestUrl: model.requestUrl
                blinksPer_kWh: blink.blinksPer_kWh
                secondsPerValue: model.secondsPerValue
            }
        }
    }


    LicenseDialog {
        id: licenseDialog
        anchors.fill: parent
        property bool showOnStart: true
        visible: showOnStart
        onDismissed: showOnStart = false;
    }

    Settings {
        property alias showLicenseInfo: licenseDialog.showOnStart
    }
}
