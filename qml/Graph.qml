/*
    * SPDX-FileCopyrightText: 2024 Kåre Särs <kare.sars@iki.fi>
    * SPDX-License-Identifier: GPL-2.0-or-later
    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with this program in a file called COPYING; if not, write to
    * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    */

import QtQuick
import QtQuick.Controls.Material
import "./" as Root


Item {
    id: graph
    implicitWidth: 800
    implicitHeight: 480

    property color barColor: "#ff007f"
    property color backgroundColor: "black"
    property color textColor: "white"
    property string requestUrl: "http://power.local/10min"
    property string label: ""
    property bool updateValues: true
    property variant values: []
    // [3400,3500,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,3400,3500,3700,3800,3900,4000,4100,4200,
    // 4300,4400,4500,4600,3500,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,3500,3700,3800,3900,4000,4100,
    // 4200,4300,4400,4500,4600,3500,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,3400,3500,3700,3800,3900,
    // 4000,4100,4200,4300,4400,4500,4600,3400,3500,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,3500,3700,
    // 3800,3900,4000,4100,4200,4300,4400,4500,4600,3500,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,3500,
    // 3700,3800,3900,4000,4100,4200,4300,4400,4500,4600]
    property date readStamp: new Date()
    readonly property double valueScale: 1 / (blinksPer_kWh * secondsPerValue / 3600.0)
    property double blinksPer_kWh: 1000
    property double secondsPerValue: 600
    property double max_kW: 20.00

    readonly property int maxNumTimeLabels: (graphItem.width / textMetrix.width) / 2
    readonly property int labelIntervalSeconds: (repeater.count * secondsPerValue) / maxNumTimeLabels
    readonly property int labelIntervalMinutes: Math.round(labelIntervalSeconds / 60.0)
    readonly property int labelIntervalHours: Math.max(Math.round(labelIntervalMinutes / 60.0), (minutesPerValue > 1 ? 1 : 0))
    readonly property int minutesPerValue: Math.floor(secondsPerValue / 60.0);
    readonly property string format: minutesPerValue > 1 ? "HH" : secondsPerValue > 1 ? "HH:mm" : "HH:mm:ss"
    // onLabelIntervalSecondsChanged: console.log("#label seconds", labelIntervalSeconds, labelIntervalMinutes, labelIntervalHours)
    // onLabelIntervalMinutesChanged: console.log("#label minutes", labelIntervalSeconds, labelIntervalMinutes, labelIntervalHours)
    // onLabelIntervalHoursChanged: console.log("#label hour", labelIntervalSeconds, labelIntervalMinutes, labelIntervalHours)

    signal error(string str)

    Text {
        id: textMetrix
        opacity: 0.0
        text: format
    }

    Rectangle { anchors.fill: parent; color: graph.backgroundColor }

    Timer {
        id: updTimer
        interval: 1000
        repeat: true
        running: graph.updateValues
        onTriggered: {
            graph.requestMinuteLoad(graph.requestUrl);
            //graph.simulate();
        }
    }

    // function simulate() {
    //     let newVals = graph.values;
    //     newVals.push(Math.floor(Math.random() * 10/graph.valueScale));
    //     while (newVals.length > 1330) {
    //         newVals = newVals.slice(1);
    //     }
    //     graph.values = newVals;
    //     readStamp = new Date();
    // }


    function requestMinuteLoad(requestUrl: string) {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    graph.values = JSON.parse(xhr.responseText.toString());
                    readStamp = new Date();
                }
                else {
                    graph.error("Failed to read data");
                    console.log("Failed to read data");
                }
            }
        }
        xhr.open('GET', requestUrl, true);
        xhr.send('');
    }


    function showTimeStamp(stamp: date) : bool {
        // We only need the timestamp as parameter, as that changes with every update
        if (labelIntervalHours > 0) { // NOTE includes minutesPerValue > 1 && labelIntervalMinutes > 30
            if (stamp.getHours() % labelIntervalHours > 0) return false;
            if (stamp.getMinutes() >= minutesPerValue) return false;
            return true;
        }

        if (minutesPerValue === 1) {
            if (labelIntervalMinutes > 10) {
                // 30 min intervals are desired
                return stamp.getMinutes() % 30 === 0;
            }
            if (labelIntervalMinutes > 5) {
                // 10 min intervals are desired
                return stamp.getMinutes() % 10 === 0;
            }
            if (labelIntervalMinutes > 1) {
                // 5 min intervals are desired
                return stamp.getMinutes() % 5 === 0;
            }
            return true;
        }


        // seconds pre value is 1-59
        if (labelIntervalMinutes > 10) {
            // 30 min intervals are desired
            return stamp.getMinutes() % 30 === 0 && stamp.getSeconds() < secondsPerValue;
        }
        if (labelIntervalMinutes > 5) {
            // 10 min intervals are desired
            return stamp.getMinutes() % 10 === 0 && stamp.getSeconds() < secondsPerValue;
        }
        if (labelIntervalMinutes > 1) {
            // 5 min intervals are desired
            return stamp.getMinutes() % 5 === 0 && stamp.getSeconds() < secondsPerValue;
        }
        if (labelIntervalSeconds > 30) {
            // 1 min intervals are desired
            return stamp.getSeconds() < secondsPerValue;
        }
        if (labelIntervalSeconds > 10) {
            // 30 sec intervals are desired
            return stamp.getSeconds() % 30 < secondsPerValue;
        }
        if (labelIntervalSeconds > 10) {
            // 30 sec intervals are desired
            return stamp.getSeconds() % 30 < secondsPerValue;
        }
        if (labelIntervalSeconds > 5) {
            // 10 sec intervals are desired
            return stamp.getSeconds() % 10 < secondsPerValue;
        }
        if (labelIntervalSeconds > 1) {
            // 5 min intervals are desired
            return stamp.getSeconds() % 5 < secondsPerValue;
        }
        return true;
    }

    Item {
        id: graphItem
        anchors {
            fill: parent
            margins: pageMargins
            bottomMargin: label2kW.height + pageMargins
        }

        Row {
            id: row
            anchors.fill: parent
            spacing: 1
            Repeater {
                id: repeater
                model: graph.values
                Rectangle {
                    anchors.bottom: parent.bottom
                    width: (row.width / graph.values.length) - row.spacing
                    height: Math.max(1, modelData * graph.valueScale * row.height / max_kW);
                    color: graph.barColor
                    Rectangle {
                        anchors {
                            top: parent.bottom
                            left: parent.left
                        }
                        height: pageMargins * 3
                        width: 1
                        color: label2kW.color
                        visible: graph.showTimeStamp(valueStamp)
                        property date valueStamp: new Date(readStamp - (repeater.count - index) * 1000 * secondsPerValue)
                        Text {
                            anchors {
                                left: parent.left
                                verticalCenter: parent.verticalCenter
                                margins: pageMargins
                            }
                            text: Qt.formatTime(parent.valueStamp, format)
                            color: graph.textColor
                        }
                    }
                }
            }
        }

        Rectangle {
            id: kW2Line
            anchors {
                left: parent.left
                right: parent.right
            }
            height: 1
            color: label2kW.color
            y: parent.height - (parent.height * 2.0 / max_kW)
        }

        Rectangle {
            id: kW5Line
            anchors {
                left: parent.left
                right: parent.right
            }
            height: 1
            color: label2kW.color
            y: parent.height - (parent.height * 5.0 / max_kW)
        }

        Rectangle {
            id: kW10Line
            anchors {
                left: parent.left
                right: parent.right
            }
            height: 1
            color: label2kW.color
            y: parent.height - (parent.height * 10.0 / max_kW)
        }

        Text {
            id: label2kW
            anchors {
                left: parent.left
                bottom: kW2Line.top
                margins: pageMargins
            }
            text: qsTr("2 kW")
            color: graph.textColor
        }

        Text {
            anchors {
                left: parent.left
                bottom: kW5Line.top
                margins: pageMargins
            }
            text: qsTr("5 kW")
            color: graph.textColor
        }

        Text {
            anchors {
                left: parent.left
                bottom: kW10Line.top
                margins: pageMargins
            }
            text: qsTr("10 kW")
            color: graph.textColor
        }

        Text {
            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
                margins: pageMargins
            }
            text: graph.label
            color: graph.textColor
        }
    }
}
